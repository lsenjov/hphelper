<html>
	<head>
		<title>Live sector updates!</title>
		<meta http-equiv="refresh" content="2">
		<style>
			body {
				font-size:300%;
			}
			td {
				text-align:right;
				font-size:300%;
			}
		</style>
	</head>
<body>
<?php
	include "../shared/hpfunctions.php";
	//Begin table
	echo "<table id=\"sector\"><tr>";
	//Table header rows
	echo "<td></td><td>C</td><td>L</td><td>S</td>";
	echo "<td style=\"width:50px;\"></td>";
	echo "<td></td><td>C</td><td>L</td><td>S</td>";
	echo "<td style=\"width:50px;\"></td>";
	echo "<td></td><td>C</td><td>L</td><td>S</td>";
	//Close header row
	echo "</tr>";
	
	hp_connect();
	$query = "SELECT live_ind_name, live_ind_current, live_ind_last, live_ind_start FROM live_ind;";
	$indicies = do_query($query);
	$counter = 0;
	echo "<tr>";
	foreach($indicies AS $value) {
		echo "<td>" . $value[0] . "</td>";
		echo "<td>" . hp_print_stat($value[1]) . "</td>";
		echo "<td>" . hp_print_stat($value[1] - $value[2]) . "</td>";
		echo "<td>" . hp_print_stat($value[1] - $value[3]) . "</td>";
		echo "<td></td>";
		if (!(++$counter % 3)) echo "</tr><tr>";
	}
	//Random news article
	echo "</table>";
	$query = "SELECT * FROM live_news;";
	$stories = do_query($query);
	echo $stories[time() / 20 % count($stories)][1];
	
	mysqli_close($_HP_CONN);
?>
</body>
</html>
