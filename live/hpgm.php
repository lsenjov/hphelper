<html>
	<head>
		<title>Welcome oh glorious GM</title>
		<script>
			function changeValue(name, num) {
				document.getElementById(name).value = parseInt(document.getElementById(name).value) + num;
			}
		</script>
	</head>

	<body>
		<form action="hpgm.php" method="post">
		<?php
			include "../shared/hpfunctions.php";
			global $_HP_CONN;
			$_HP_CONN = mysqli_connect("localhost","gm","","hphelper");
			if (mysqli_errno($_HP_CONN)) {
				echo "Could not connect to database: " . mysqli_error();
			}

			if (array_key_exists("CI", $_POST) && !array_key_exists("reset", $_POST)) {
				//Write data to database FIRST
				$query = "SELECT live_ind_name, live_ind_current FROM live_ind ORDER BY live_ind_id ASC;";
				$columns = do_query($query);
				$total = 0;
				foreach(array("CI","HI","LI","SI") as $value) {
					//Giggle indicies before normalising
					$_POST[$value] += mt_rand(-1, 1);
					//Normalise indicies
					$total += $_POST[$value];
				}
				$total = floor($total / -4);
				foreach(array("CI","HI","LI","SI") as $value) {
					//Normalise indicies
					$_POST[$value] += $total;
				}
				//Indicies complete, now on to service groups

				$total = 0;
				$count = 0;
				foreach($columns as $value) {
					if (in_array($value[0], array("CI","HI","LI","SI"))) continue;//One of the indicies, ignore
					//Jiggle service group before normalising
					$_POST[$value[0]] += mt_rand(-1, 1);
					//Is not an index, time to add
					++$count;
					$total += $_POST[$value[0]];
				}
				$total = round($total / -$count);
				foreach($columns as $value) {
					if (in_array($value[0], array("CI","HI","LI","SI"))) continue;//One of the indicies, ignore
					//Normalise service groups
					$_POST[$value[0]] += $total;
				}
				//Update last
				foreach($columns as $value) {
					$query = "UPDATE `live_ind` SET `live_ind_last`='".$value[1]."' WHERE `live_ind_name` LIKE \"".$value[0]."\";";
					if (!mysqli_query($_HP_CONN, $query)) echo "Query ".$query." failed: ".mysqli_error($_HP_CONN);
				}
				//Update current
				foreach($columns as $value) {
					$query = "UPDATE `live_ind` SET `live_ind_current`='".$_POST[$value[0]]."' WHERE `live_ind_name` LIKE \"".$value[0]."\";";
					if (!mysqli_query($_HP_CONN, $query)) echo "Query ".$query." failed: ".mysqli_error($_HP_CONN);
				}
			} else if (array_key_exists("reset", $_POST)) {
				//Get columns of page
				$query = "SELECT live_ind_name FROM live_ind ORDER BY live_ind_id ASC;";
				$columns = do_query($query);
				foreach($columns as $value) {
					//Update all column values to currently set ones
					$query = "UPDATE `live_ind` SET `live_ind_last`='".$_POST[$value[0]]."', `live_ind_current`='".$_POST[$value[0]]."', `live_ind_start`='".$_POST[$value[0]]."' WHERE `live_ind_name` LIKE '".$value[0]."';";
					if (!mysqli_query($_HP_CONN, $query)) echo "Query ".$query." failed: ".mysqli_error($_HP_CONN);
				}
			}

			//Time to create the page
			echo "<table border=\"1\"><tr>";
			$query = "SELECT live_ind_name, live_ind_current FROM live_ind ORDER BY live_ind_id ASC;";
			$columns = do_query($query);

			//Create Headers
			foreach($columns as $value) {
				echo "<td>".$value[0]."</td>";
			}
			echo "</tr><tr>";
			//Create up buttons
			foreach(Array(10,5,3,1) as $inc) {
				foreach($columns as $value) {
					echo "<td>";
						echo "<button type=\"button\" onclick=\"changeValue('".$value[0]."',$inc)\">+".$inc.$value[0]."</button>";
					echo "</td>";
				}
				echo "</tr><tr>";
			}
			//Create display values in middle
			foreach($columns as $value) {
				echo "<td><input type=\"text\" size=\"3\" id= \"".$value[0]."\" name=\"".$value[0]."\" value=\"".$value[1]."\" /></td>";
			}
			echo "</tr><tr>";

			//Create down buttons
			foreach(Array(-1, -3, -5, -10) as $inc) {
				foreach($columns as $value) {
					echo "<td>";
						echo "<button type=\"button\" onclick=\"changeValue('".$value[0]."',$inc)\">".$inc.$value[0]."</button>";
					echo "</td>";
				}
				echo "</tr><tr>";
			}

			echo "</tr></table>";
			echo "<input type=\"submit\" value=\"Push to Server\" />";
			echo "<br /><br /><br />";
			echo "<input type=\"checkbox\" name=\"reset\">Reset<br />";
			mysqli_close($_HP_CONN);
		?>
		</form>
	</body>
</html>
