<html>
	<head>
		<title>Random news article</title>
	</head>

	<body>
		<div id="story">
			<?php
				include "../shared/hpfunctions.php";
				include "../config/hpvars.php";
				hp_connect();
				
				$query = "SELECT COUNT(news.news_id) FROM news WHERE news.c_id IS NULL;";
				$count = do_query($query)[0][0];
				
				$query = "SELECT news.news_desc FROM news WHERE news.c_id IS NULL LIMIT 1 OFFSET ".mt_rand(0, $count - 1).";";
				$result = do_query($query)[0][0];
				hp_echo($result, 0);
				
				mysqli_close($_HP_CONN);
			?>
		</div>
	</body>
</html>
