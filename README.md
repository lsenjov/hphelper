# README #

Quick Readme creation

### What is this repository for? ###

* Quick summary
The HPHelper is a series of tools for running PARANOIA: High Programmers.

* Scenario Generator
Creates a ready-made scenario to be run with players.
Version 0.2

* Live Sector Updates
Used to display sector updates, modified live by the gamemaster.
Version 0.1

* Character Generator
Creates a random high programmer for a player to use.
Version 0.1

### How do I get set up? ###

* Summary of set up
Put the repository in your web server folder and create links to hpform.php and hplive.php. hpgm.php is required for use with hplive.php, but should not be directly hyperlinked.
* Dependencies
mysql/mariadb
* Database configuration
Using mysql/mariadb, create a database called hphelper then use the sql file to create the database. Create two users: 'fc' and 'gm' at localhost without a password (this can be edited) who access the database. 'fc' requires SELECT on hphelper.*, and gm requires SELECT and UPDATE on live_ind and live_news.
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
