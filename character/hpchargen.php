<?php
  include "../shared/hpfunctions.php";

  function print_drawback() {
    $query = "select text from drawbacks;";
    $result = do_query($query);
    hp_echo($result[rand(0, count($result) - 1 ) ][0], rand(0, 30) );
  }
?>

<html>
  <head>
    <title>Character Generator</title>
    <style>
      body {
        width : 960px;
      }
    </style>
  </head>
  <body>
    <?php
      hp_generate_char();
    ?>
  </body>
</html>
