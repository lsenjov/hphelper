<?php
	if (!defined("HP_NUM_PLAYERS")) {
		define("HP_NUM_PLAYERS", 6);
		define("HP_DATABASE_USERNAME", "fc");
		define("HP_DATABASE_PASSWORD", "");
		define("HP_DATABASE_ADDRESS", "localhost");
		define("HP_SEPARATOR", " -- ");
		define("HP_NUM_CRISIS", 3);
		define("HP_NUM_MINIONS", 10);
        define("HP_CHARS_PER_SHEET", 1);

		//Used in case of too few matching objects in database. Shouldn't need to change it unless HP_NUM_CRISIS is set too high
		define("HP_TIMEOUT", 40);

		//How much to adjust an index per tag. Higher means larger jumps
		define("HP_INDEX_ADJUST", 10);

		//How much the adjustment can vary. Higher means more randomness and less associating to crisis tags. Is also applied once at beginning for a statline that doesn't look "3 3 3 3 -7 3 3"
		define("HP_INDEX_VARIATION", 4);

		//If true, allow duplicate resources and locations across Crisises. If true, the same location or resource will not be called for different crisises
		define("HP_ALLOW_DUPLICATES", false);

		//Minimum news stories
		define("HP_NUM_NEWS_STORIES", 10);
	}
?>
