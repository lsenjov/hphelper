<?php
include "../config/hpvars.php";

function do_query($query) {
  //Helper function that compresses the long sqli_fetch_all(mysqli_query
  //Uses a defined $_HP_CONN
  global $_HP_CONN;
  if ( (!isset($_HP_CONN)) || (!$_HP_CONN)) {
    hp_connect();
  }

  if (!_HP_CONN) {
    error_log("Could not connect to database in do_query()");
  }

  // So mysqli_fetch_all is a bad idea, but so I don't have to
  // re-write everything, I'm just gonna put it into an array anyway
  $result = mysqli_query($_HP_CONN, $query);
  $out = array();
  $count = 0;
  while ($row = mysqli_fetch_row($result)) {
    $out[$count] = $row;
    ++$count;
  }
  return $out;
}

function hp_choose_directives() {
  //Goes through the crisises and returns a list of randomly picked ones
  //It will first add a normal or blockbuster crisis
  //Then add crisises until it reaches HP_NUM_CRISIS in hpvars.php

  //list of Crisises to return
  $crisisList = array();
  //It will only ever add a max of one blockbuster crisis
  $blockbusterFound = false;
  
  //Count number of applicable crisises for first step
  $query = "SELECT COUNT(crisis.c_id) FROM crisis WHERE c_type != 'bickering';";
  $count = do_query($query)[0][0];
  //Get random query from set
  $query = "SELECT crisis.c_id, crisis.c_type FROM crisis WHERE c_type != 'bickering' LIMIT 1 OFFSET " . mt_rand(0, $count - 1);
  //Adds Crisis id to list
  $result = do_query($query)[0];
  $crisisList[0] = $result[0];
  //First crisis is in, check for blockbuster now
  if ($result[1] == "blockbuster") $blockbusterFound = true;

  //Time to add others
  $timeout = 0;
  while (count($crisisList) < HP_NUM_CRISIS && $timeout++ < HP_TIMEOUT) {
    if ($blockbusterFound) {
      $query = "SELECT crisis.c_id, crisis.c_type FROM crisis WHERE c_type != 'blockbuster';";
    } else {
      $query = "SELECT crisis.c_id, crisis.c_type FROM crisis;";
    }
    $result = do_query($query);
    $candidate = $result[mt_rand(0, count($result) - 1)];//Find random crisis
    if (in_array($candidate[0], $crisisList)) continue;//Check if already in list
    $crisisList[count($crisisList)] = $candidate[0];//Add to list of crisises
    if ($candidate[1] == "blockbuster") $blockbusterFound = true;//Checking for bb
  }

  return $crisisList;
}

function hp_populate_directives($crisisList) {
  //Returns an array of Directives, according to the array of integers given by $crisisList
  $directiveList = array();
  for ($crisisNum = 0; $crisisNum < count($crisisList); ++$crisisNum) {
    $query = "SELECT sg.sg_id, sgm.sgm_text, sg.sg_name, sgm.c_id FROM sgm, sg WHERE sgm.c_id = " . $crisisList[$crisisNum] . " AND sgm.sg_id = sg.sg_id;";
    $result = do_query($query);
    for ($row = 0; $row < count($result); ++$row) {
      $directiveList[count($directiveList)] = $result[$row];
    }
  }
  //Now need to get directives for those without
  $query = "SELECT sg.sg_id FROM sg;";
  $serviceGroups = do_query($query);
  foreach ($serviceGroups as $group) {
    foreach($directiveList as $directive) {
      if ($group[0] == $directive[0]) continue 2;//Found group, continue
    }
    //Group not found in directives, add random non-crisis directive
    $query = "SELECT sg.sg_id, sgm.sgm_text, sg.sg_name, sgm.c_id FROM sgm, sg WHERE sgm.c_id IS NULL AND sgm.sg_id = " . $group[0] . " AND sgm.sg_id = sg.sg_id;";
    $result = do_query($query);
    $directiveList[count($directiveList)] = $result[mt_rand(0, count($result)-1)];
    $directiveList[count($directiveList)-1][3] = 0; //Generic directive, must set to 0
  }

  return $directiveList;
}

function hp_populate_society_missions($crisisList) {
  //Returns a list of mission IDs, according to the array of integers given by $crisisList
  $missionList = array();
  for ($crisisNum = 0; $crisisNum < count($crisisList); ++$crisisNum) {
    $query = "SELECT ssm.ss_id, ssm.ssm_text, ss.ss_name, ssm.c_id FROM ssm, ss WHERE ssm.c_id = " . $crisisList[$crisisNum] . " AND ssm.ss_id = ss.ss_id;";
    $result = do_query($query);
    for ($row = 0; $row < count($result); ++$row) {
      $missionList[count($missionList)] = $result[$row];
    }
  }

  //Must now check for societies without missions
  $query = "SELECT ss.ss_id FROM ss;";
  $societies = do_query($query);
  foreach ($societies AS $society) {
    foreach($missionList AS $mission) {
      if ($society[0] == $mission[0]) continue 2;//Found society, continue on
    }
    //Society not found in mission list, add random non-crisis mission
    $query = "SELECT ssm.ss_id, ssm.ssm_text, ss.ss_name FROM ssm, ss WHERE ssm.c_id IS NULL AND ssm.ss_id = " . $society[0] . " AND ssm.ss_id = ss.ss_id;";
    $result = do_query($query);
    $missionList[count($missionList)] = $result[mt_rand(0, count($result)-1)];
    $missionList[count($missionList)-1][3] = 0;//Generic mission, must set to 0
  }

  return $missionList;
}

function hp_print_portfolios() {
  //Create Service Group Portfolios
  $query = "SELECT * FROM sg;";
  $serviceGroups = do_query($query);
  //for ($sg = 0; $sg < count($serviceGroups); ++$sg) {//For each service group
  $sgCounter = 0;
  foreach ($serviceGroups AS $sg) {
    echo "<h3 ";
    if (!($sgCounter++ % 2)) {//For the first and every other service group
      echo "style=\"page-break-before: always;\"";
    }
    
    echo ">" . $sg[2] . "</h3>";//Print header
    //Skills required are:
    $query = "SELECT sg_skill.skills_id FROM sg_skill WHERE sg_id = " . $sg[0] . ";";
    $skillsList = do_query($query);
    $minions = array();

    for ($skillNum = 0; $skillNum < count($skillsList); ++$skillNum) {//For each skill
      //Find a minion in the service group with the skill
      $query = "SELECT minion.minion_id FROM minion, minion_skill WHERE minion.sg_id = " . $sg[0] . " AND minion_skill.minion_id = minion.minion_id AND minion_skill.skills_id = " . $skillsList[$skillNum][0] . ";";
      $candidates =do_query($query);//Found list of candidates
      //Are there any candidates?
      if (count($candidates) == 0) continue;

      $candidate_num = $candidates[mt_rand(0, count($candidates) - 1)][0];

      //See if that minion exists in $minions already
      if (in_array($candidate_num, $minions)) continue;
      //If not, add the minion
      $minions[count($minions)] = $candidate_num;
    }
    //Make sure programmers have at least HP_NUM_MINIONS minions each
    if (count($minions) < HP_NUM_MINIONS) {
      $query = "SELECT minion.minion_id FROM minion WHERE minion.sg_id = " . $sg[0] . ";";
      $result = do_query($query);
      $timeout = 0;//Safety valve
      while (count($minions) < HP_NUM_MINIONS && $timeout++ < HP_TIMEOUT) {
        $m = $result[mt_rand(0, count($result) - 1)][0];
        if (in_array($m, $minions)) continue;//Minion already there, get a new one
        $minions[count($minions)] = $m;
      }
    }
    //Candidate numbers complete
    //Sort numbers
    sort($minions, SORT_NUMERIC);

    //Print minions
    echo "<small>";
    //Put it in a 2 column table
    echo "<table border=\"1\">";
    $counter = 1;
    foreach ($minions as $minion) {
      if (!(++$counter % 2)) echo "<tr>";
      $query = "SELECT * FROM minion_skills WHERE minion_id = " . $minion . ";";
      $result = do_query($query)[0];
      echo "<td>" . $result[1] . HP_SEPARATOR . $result[2] . HP_SEPARATOR . $result[3] . HP_SEPARATOR . $result[4] . "</td>";
      if ($counter % 2) echo "</tr>";
    }
    echo "</table></small>";
  }
}

function hp_index_normalise($indicies) {
  //Takes an array of integers. Normalises the first four to a mean average of 0
  //Does the same with the rest of the elements

  //Normalise first four
  $total = 0;

  //Create a slice of the array with just the first four
  $slice = array_slice($indicies, 0, 4);

  //Find total
  foreach ($slice as $key => $value) {
    $total += $value;
  }
  //Find normaliser, just re-using $total
  $total /= -count($slice);

  //Round it to make it appear nicer
  $total = round($total);
  
  //For each row in the slice
  foreach ($slice as $key => $value) {
    //Change the $indicies value
    $indicies[$key] += $total;
  }
  

  //Normalise first four
  $total = 0;

  //Create a slice of the array with just the first four
  $slice = array_slice($indicies, 4);

  //Find total
  foreach ($slice as $key => $value) {
    $total += $value;
  }
  //Find normaliser, just re-using $total
  $total /= -count($slice);

  //Round it to make it appear nicer
  $total = round($total);
  
  //For each row in the slice
  foreach ($slice as $key => $value) {
    //Change the $indicies value
    $indicies[$key] += $total;
  }

  return $indicies;
}

function hp_calculate_statline($crisisList) {
  //Create an array containing the four indicies, then all the service groups
  $indicies = array( "HI" => 0, "SI" => 0, "CI" => 0, "LI" => 0);
  $query = "SELECT sg.sg_abbr FROM sg";
  //Get service groups abbreviations
  $result = do_query($query);
  //Populate list with service groups
  for ($i = 0; $i < count($result); ++$i) {
    $indicies[$result[$i][0]] = 0;
  }
  
  //For each of the crisises
  for ($cNum = 0; $cNum < count($crisisList); ++$cNum) {
    $query = "SELECT ct_tag FROM crisis_tag WHERE c_id = " . $crisisList[$cNum] . ";";
    //Get crisis tags
    $result = do_query($query);
    while ($tag = each($result)) {//For each tag
      $tag = $tag["value"][0];
      //Check if index exists first. If not, continue loop
      if (!array_key_exists(substr($tag,0,2), $indicies)) continue;
      switch ($tag[2]) {
        case 'U':
          $indicies[substr($tag,0,2)] += HP_INDEX_ADJUST + mt_rand(-HP_INDEX_VARIATION, HP_INDEX_VARIATION);
          break;
        case 'U':
          $indicies[substr($tag,0,2)] -= HP_INDEX_ADJUST + mt_rand(-HP_INDEX_VARIATION, HP_INDEX_VARIATION);
          break;
      }
    }
  }
  //Create additional uncertainty
  foreach ($indicies AS $key => $value) $indicies[$key] += mt_rand(-HP_INDEX_VARIATION, HP_INDEX_VARIATION);
  $indicies = hp_index_normalise($indicies);
  return $indicies;
}

//Returns a single number, adding a directional arrow at the beginning
function hp_print_stat($value) {
  $string = "";
  if ($value > 0) {
    $string .= "&#8657;";//Up arrow
  } else if ($value < 0) {
    $string .= "&#8659;";//Down arrow
  } else $string .= "&#8658;";//Right arrow
  $string .= abs($value);
  return $string;
}

function hp_print_statline($indicies) {
  foreach ($indicies AS $key => $value) {
    echo $key . hp_print_stat($value) . " ";
  }
  echo "<br />";
}

function hp_print_crisis($crisisNum) {
  //Takes an integer value as $crisisNum, and prints the Crisis description
  //Along with each bit of text associated
  $query = "SELECT crisis.c_desc FROM crisis WHERE crisis.c_id = " . $crisisNum . ";";
  $result = do_query($query);
  //Should only return 1 row per crisis for this query
  hp_echo("<b>" . $result[0][0] . "</b><br />", $crisisNum);

  $query = "SELECT crisis_text.ct_desc FROM crisis_text WHERE c_id = " . $crisisNum . ";";
  $result = do_query($query);
  foreach ($result AS $value) {
    hp_echo($value[0] . "<br />", $crisisNum);
  }
}

function hp_print_player_sheet($pNum, $missionList, $indicies, $news) {
  if (strlen($_POST["name_".$pNum]) == 0) return;//If no name, return
  echo "<h2 ";
  //Force new page for every second sheet
  if (!($pNum % HP_CHARS_PER_SHEET)) echo "style=\"page-break-before: always;\"";
  echo ">Welcome High Programmer: ";
  hp_echo($_POST["name_".$pNum] . "</h2>", 0);
  hp_print_statline($indicies);

  //Print news
  echo "<b>Sector news summary</b><br />";
  echo "<small>";
  hp_echo("Todays news brought to you by: ".hp_get_ad(), 0);
  foreach($news as $article) {
    hp_echo("<br />");
    echo HP_SEPARATOR;
    hp_echo($article, 0);
  }
  echo "</small><br />";

  //Print secret society missions
  echo "<b>Message summary follows:</b><br />";
  echo "<small>";
  hp_echo("Today's messages brought to you by: ".hp_get_ad()."<br />", 0);
  foreach($missionList as $mission) {
    if (isset($_POST["ss_".$pNum."_".$mission[0]])) {
      //Player has this secret society on, print mission
      hp_echo($mission[2] . HP_SEPARATOR . $mission[1] . "<br />", $mission[3]);
    }
  }

  //Print additional solo messages
  $messages = explode('\n', $_POST["messages_".$pNum]);
  foreach($messages AS $line) {
    if (trim(strlen($line))) hp_echo($line."<br />",0);//Making sure line is not empty
  }
  echo "</small><br />";

  // Print Character Sheet
  if (isset($_POST["genchar_".$pNum])) {
    echo "<iframe src=\"../character/hpchargen.php\" scrolling=\"no\" frameborder=\"0\" width=\"960\" height=\"450\"> </iframe>";
    echo "<br />";
  }
}

function hp_compare_zeroth ($a, $b) {
  //Only for use when sorting via first index
  return $a[0]-$b[0];
}

function hp_add_name($givenName) {
  //$givenName is string in form of CIT-V-1-0
  //CIT is the substitution type (CITizen, RESource, LOCation)
  //V is clearance (can be 2 letters "IR"), only appears on citizens
  //1 is which citizen
  //And 0 is the crisis number. Crisis 0 is for generic missions/directives
  global $_HP_NAMES;
  if (!isset($_HP_NAMES)) $_HP_NAMES = array();
  //Clearance is IR-R-O-Y-G-B-I-V-U
  $clearance = explode("-", $givenName)[1];
  $query = "SELECT name.name_first, name.name_clearance, name.name_zone, name.name_clone FROM name WHERE name.name_clearance = \"".$clearance."\";";
  $names = do_query($query);
  if (!count($names)) return false;//No names exist, probably invalid $clearance
  
  do {
    $name = $names[mt_rand(0, count($names)-1)];
    $name = $name[0].'-'.$name[1].'-'.$name[2];
  } while (in_array($name, $_HP_NAMES));//Do while the name is already chosen
  $_HP_NAMES[$givenName] = $name;
  
  return $givenName;//Return key when successful
}

function hp_add_resource($name) {
  //name will be in the form of RES-1-0
  //RES is the type (useful only for the query)
  //1 is which resource (possible multiple resources)
  //0 is the crisis number. Crisis 0 is for generic missions/directives
  global $_HP_NAMES;
  if (!isset($_HP_NAMES)) $_HP_NAMES = array();
  $query = "SELECT resource.resource_name FROM resource WHERE resource.resource_type = \"".explode('-', $name)[0]."\";";
  $resources = do_query($query);
  if (!count($resources)) return false;//No resources exist. Fail.

  do {
    $rName = $resources[mt_rand(0, count($resources)-1)][0];
  } while (!HP_ALLOW_DUPLICATES && in_array($rName, $_HP_NAMES));
  $_HP_NAMES[$name] = $rName;

  return $name;
}

function hp_add_secret_society($name) {
  //name will be in the form of SOC-1-0
  //SGF is the type (useful only for the query)
  //1 is which resource (possible multiple resources)
  //0 is the crisis number. Crisis 0 is for generic missions/directives
  global $_HP_NAMES;
  if (!isset($_HP_NAMES)) $_HP_NAMES = array();
  $query = "SELECT ss.ss_name FROM ss;";
  $possibles = do_query($query);
  if (!count($possibles)) return false;//No possibilities exist. Fail.
  do {
    $rName = $possibles[mt_rand(0, count($possibles)-1)][0];
  } while (!HP_ALLOW_DUPLICATES && in_array($rName, $_HP_NAMES));
  $_HP_NAMES[$name] = $rName;

  return $name;
}

function hp_add_service_firm($name) {
  //name will be in the form of SGF-1-0
  //SGF is the type (useful only for the query)
  //1 is which resource (possible multiple resources)
  //0 is the crisis number. Crisis 0 is for generic missions/directives
  global $_HP_NAMES;
  if (!isset($_HP_NAMES)) $_HP_NAMES = array();
  $query = "SELECT sf.sf_name FROM sf;";
  $firms = do_query($query);
  if (!count($firms)) return false;//No resources exist. Fail.
  do {
    $rName = $firms[mt_rand(0, count($firms)-1)][0];
  } while (!HP_ALLOW_DUPLICATES && in_array($rName, $_HP_NAMES));
  $_HP_NAMES[$name] = $rName;

  return $name;
}

function hp_get_name($name) {
  //At the moment ignores clone numbers and returns a random clone number between 1 and 9
  global $_HP_NAMES, $_HP_ZONE;
  if (!isset($_HP_NAMES)) $_HP_NAMES = array();
  if (!array_key_exists($name, $_HP_NAMES)) {
    switch (explode('-', $name)[0]) {//Get name type
      case "CIT":
        hp_add_name($name);
        break;
      case "ZON":
        return $_HP_ZONE;
      case "PLA":
        return $_POST["name_".explode('-',$name)[1]];
      case "RES": case "LOC":
        hp_add_resource($name);
        break;
      case "SGF":
        //Service group firm
        hp_add_service_firm($name);
        break;
      case "SOC":
        // Random secret society
        hp_add_secret_society($name);
        break;
      default:
        error_log("Unknown item name type: ".$name);
        return $name;//Either not a tag or mistyped, return name normally
    }
    return hp_get_name($name);
  }//Name is definately in _HP_NAMES
  return $_HP_NAMES[$name];
}

function hp_echo($line, $crisis) {
  //Echos hp_replace.
  echo hp_replace($line, $crisis);
}

function hp_replace($line, $crisis) {
  //Takes a line, substitutes all names in the line
  //Number of crisis is required to make sure names and objects line up
  while (strpos($line, "##") !== false) {
    //Temp is a temporary array of an exploded line
    $temp = explode("##", $line, 3);
    
    $temp[1] = hp_get_name($temp[1]."-".$crisis);
    $line = implode($temp);
  }
  //Line is either invalid or all tags have been taken care of
  return $line;
}

//Connect to the database, store connection as global $_HP_CONN
function hp_connect() {
  $hpConn = mysqli_connect(HP_DATABASE_ADDRESS, HP_DATABASE_USERNAME, HP_DATABASE_PASSWORD, "hphelper");
  if (mysqli_connect_errno()) {
    echo "Failed to connect to database: " . mysqli_connect_error();
  }
  global $_HP_CONN;
  $_HP_CONN = $hpConn;
}

//Makes a random zone name
function hp_make_zone_name() {
  $zone = "";
  for ($i = 0; $i < 3; ++$i) {
    $zone = $zone."ABCDEFGHIJKLMNOPQRSTUVWXYZ"[mt_rand(0, 25)];
  }
  return $zone;
}

//Populate news list, plus a few extra news articles
function hp_populate_news($crisisList) {
  $newsList = array();
  //Add crisis specific news stories
  foreach ($crisisList as $crisisNum) {
    $query = "SELECT `news_desc` FROM `news` WHERE `c_id`=".$crisisNum.";";
    $result = do_query($query);
    foreach($result AS $row) {
      $newsList[count($newsList)] = hp_replace($row[0], $crisisNum);
    }
  }

  //Add additional random articles
  $query = "SELECT `news_desc` FROM `news` WHERE `c_id` IS NULL;";
  $result = do_query($query);

  $timeout = 0;
  while ($timeout++ < HP_TIMEOUT && count($newsList) < HP_NUM_NEWS_STORIES) {
    $rand = mt_rand(0, count($result)-1);
    if (in_array(hp_replace($result[$rand][0], 0), $newsList)) continue;//Already exists, find next random
    $newsList[count($newsList)] = hp_replace($result[$rand][0], 0);
  }

  //Randomize order
  shuffle($newsList);

  //Return filled randomized news list
  return $newsList;
}

//Gets a random advertisement
function hp_get_ad() {
  $query = "SELECT COUNT(`sf_id`) FROM `sf`;";
  $result = do_query($query);
  $count = $result[0][0];

  $query = "SELECT `sf_name`,`sf_ad` FROM `sf` LIMIT 1 OFFSET ".mt_rand(0, $count-1).";";
  $row = do_query($query)[0];
  return $row[0].HP_SEPARATOR.$row[1];
}

function hp_print_page() {
  //Inialise zone name
  global $_HP_ZONE;
  $_HP_ZONE = $_POST["s_name"];
  if (!(strlen($_HP_ZONE))) {//Zone is empty
    $_HP_ZONE = hp_make_zone_name();
  }


  //Get Seed
  $seed = $_POST["seed"];
  if (!(strlen($seed) && is_numeric($seed))) {
    //If seed does not exist or is not a number, generate new seed
    $seed = mt_rand(0, 100000);
  }

  //Seed random number generator
  mt_srand($seed);
  hp_echo("<h1>Welcome GM to sector ".$_HP_ZONE.", seed is: " . $seed . "</h1><br />", 0);

  //Initialise Name Array. These include characters from INFRARED up to ULTRAVIOLET
  global $_HP_NAMES;
  $_HP_NAMES = array();

  include "hpvars.php";
  //Connect to Database
  hp_connect();
  //Stuff it, database connection is going global. Will eventually re-factor script to take advantage
  global $_HP_CONN;

  //Choose Crisises
  //$crisisList is an array of crisis IDs
  $crisisList = hp_choose_directives();

  //Populate Directives
  //Directives list in each row: sg_id, Text, SG Name
  $directiveList = hp_populate_directives($crisisList);
  //Sort by sg_id for grouping
  usort($directiveList, "hp_compare_zeroth");

  //Populate Secret Society Missions
  //Mission list in each row: ss ID number, Mission text, ss Name
  $missionList = hp_populate_society_missions($crisisList);
  //Sort by id for ease of reference
  usort($missionList, "hp_compare_zeroth");

  //Create statline
  $indicies = hp_calculate_statline($crisisList);

  //Create news
  $news = hp_populate_news($crisisList);

  //Print GM's Sheet
  //Print Statline
  hp_print_statline($indicies);
  //Print Crisises and Text
  foreach($crisisList as $value) hp_print_crisis($value);
  //Print directives for reading, make them small
  echo "<b>Printing Directives</b><br /><small>";
  foreach($directiveList as $value) {
    hp_echo($value[2] . ": " . $value[1] . "<br />", $value[3]);
  }
  echo "</small>";

  //Print news articles
  echo "<b>Printing sector news</b><br />";
  echo "<small>";
  foreach($news as $article) {
    hp_echo($article.HP_SEPARATOR, 0);
  }
  echo "</small>";

  //Print society missions for reading, make them small
  echo "<br /><b>Printing Secret Society Missions</b><br /><small>";
  foreach($missionList as $value) {
          if (!$value[1]) {
              continue;
          }
    hp_echo($value[2] . ": " . $value[1] . HP_SEPARATOR, $value[3]);
  }
  echo "</small>";

  //Print individual high programmer sheets
  for ($p = 0; $p < HP_NUM_PLAYERS; ++$p) {
    hp_print_player_sheet($p, $missionList, $indicies, $news);
  }

  //Print randomised service group portfolios
  hp_print_portfolios();

  //Print Directives for Distribution
  echo "<b style=\"page-break-before: always\">Printing Directives for Distribution</b>";
  $tableline = true;
  echo "<table border=\"1\"><tr>";
  foreach($directiveList as $value) {
    $tableline = !$tableline;
    echo "<td>";
    hp_echo($value[2] . ": " . $value[1], $value[3]);
    echo "</td>";
    if ($tableline) echo "</tr><tr>";
  }
  echo "</tr></table>";

  //Finally close connection
  mysqli_close($_HP_CONN);
}

function hp_generate_char() {
  $statmin = 1;
  $statmax = 20;

  // Six vars for the six stats
  $hpcg_stats["management"] = rand($statmin, $statmax);
  $hpcg_stats["subterfuge"] = rand($statmin, $statmax);
  $hpcg_stats["violence"] = rand($statmin, $statmax);
  $hpcg_stats["hardware"] = rand($statmin, $statmax);
  $hpcg_stats["software"] = rand($statmin, $statmax);
  $hpcg_stats["wetware"] = rand($statmin, $statmax);

  $hpcg_public = 0;

  $hpcg_remain = 100;
  foreach ($hpcg_stats as $hpcg_val) {
    $hpcg_remain -= $hpcg_val;
  }

  if ($hpcg_remain > 30) {
    $hpcg_public = rand(5, 10);
    $hpcg_total -= 2 * $hpcg_public;
  }

  // Print stats
  foreach ($hpcg_stats as $key => $val) {
    echo $key . ": " . $val . "<br />";
  }
  echo "<br />";

  // Secondary Stats
  $progGroupSize = ceil($hpcg_stats["management"] / 5 + 1);
  echo "Program group size: " . $progGroupSize . "<br />";
  for ($i = 0; $i < $progGroupSize; ++$i) {
    hp_echo("##SOC-" . $i . "##<br />");
  }
  echo "Clone degredation: " . (5 - ceil($hpcg_stats["wetware"] / 5)) . "<br />";

  // Public Standing
  echo "Public Standing: ";
  if ($hpcg_public > 0) {
    echo $hpcg_public;
  } else {
    echo "none";
  }
  echo "<br />";

  // Drawbacks
  echo "<br />";
  for ($i = 0; $i < 3; ++$i) {
    if ($hpcg_remain < 30) {
      print_drawback();
      echo "<br />";
      $hpcg_remain += 10;
    }
  }

  echo "Access Remaining: " . $hpcg_remain . "<br />";
}

?>
