<html>
	<head>
		<title>High Programmer Helper</title>
	</head>

	<body>
		<h1>High Programmer Helper</h1>
		As some notes: <ul>
			<li>You can use a double hash to sub objects:</li>
			<li>##LOC-1## for example, pulls a random location. ##LOC-1## is used for many of the generic SS missions, and is often a good pick.</li>
			<li>##CIT-G-1## will give you a random GREEN citizen's name. These names work from R all the way up to U</li>
			<li>##RES-1## will give a random resource. Anything from Nuclear Waste to Hot Fun. Rare in the missions, but may be useful</li>
			<li>##ZON## will return the zone's three letters.</li>
			</ul>
		<form action="hpprint.php" method="post">
			Random Seed: <input type="text" name="seed" />(Numeric only, Leave blank for random)<br />
			Sector Name: <input type="text" name="s_name" />(Leave blank for random)<br />

			<?php
				include "../config/hpvars.php";
				$conn = mysqli_connect(HP_DATABASE_ADDRESS, HP_DATABASE_USERNAME,HP_DATABASE_PASSWORD,"hphelper");
				if (mysqli_connect_errno()) {
					echo "Failed to connect to database: " . mysqli_connect_error();
				}

				//Get secret societies
				$query = "SELECT ss.ss_id, ss.ss_name FROM ss;";

				echo "<table><tr>";//Begin table
				for ($p = 0; $p < HP_NUM_PLAYERS; ++$p) {//Creating individual hp profiles
					echo "<td>";
					echo "Name:";
					echo "<input type=\"text\" name=\"name_" . $p . "\" /><br />";
          echo "<input type=\"checkbox\" name=\"genchar_" . $p . "\" />Random Char?<br />";


          $result = mysqli_query($conn, $query);
					while ($row = mysqli_fetch_row($result)) { //Create secret society checkboxes
						echo $row[1];//Society name
						echo "<input type=\"checkbox\" name=\"ss_" . $p . '_' . $row[0] . "\" /><br />";
					}
					echo "Individual private messages for Player:";
					echo "<textarea rows=\"8\" name=\"messages_" . $p . "\"></textarea><br />";
					echo "</td>";
				}
				echo "</tr></table>";

				//Close Connection
				mysqli_close($conn);
			?>
			<input type="submit" value="Create sector" />
		</form>
	</body>
</html>
